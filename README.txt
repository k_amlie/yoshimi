V 1.5.10.2

One of the libraries Yoshimi depends on is in the process of upgrading, and will lose some - now deprecated - access points. In view of this we've decided to make a pre-emptive bugfix release. At the same time, we've prevented some memory leaks that might have occurred when handling corrupted files.


Yoshimi source code is available from either:
https://sourceforge.net/projects/yoshimi
Or:
https://github.com/Yoshimi/yoshimi

Full build instructions are in 'INSTALL'.

Our list archive is at:
https://www.freelists.org/archive/yoshimi
To post, email to:
yoshimi@freelists.org
